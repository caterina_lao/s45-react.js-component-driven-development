import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){


	const { user } = useContext(UserContext);
	//State hooks to store the values of the input fields
	const [ firstName, setFirstName] = useState('')
	const [ lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	//State to determine whether register button is enabled or not

	const [isActive, setIsActive] = useState(false);

	console.log(firstName)
	console.log(lastName)
	console.log(email)
	console.log(mobileNumber)
	console.log(password1)
	console.log(password2)




	function registerUser(e) {

		//Prevents page redirection via from submission
		e.preventDefault();

		// localStorage.setItem('email', email);

  //   	setUser({
  //       	email: localStorage.getItem('email')
  //   	})
		//Clear the input fields
	// 	setEmail('');
	// 	setPassword1('');
	// 	setPassword2('');

	// 	alert('Thank you for registering!')
	// }

	fetch(`http://localhost:4000/users/register`, {
        method: 'POST',
        headers: {
            "Content-Type" : "application/json"
        },
        body: JSON.stringify({
        	firstName: firstName,
        	lastName: lastName,
            email: email,
            mobileNumber: mobileNumber,
            password: password
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data)

        if(data === true && data.email !== email){
				Swal.fire({
					title: 'Registration successful',
					icon: 'success',
					text: 'Welcome to Zuitt'
				})

			}else if(data.email === email){
				Swal.fire({
					title: 'Duplicate email found',
					icon: 'error',
					text: 'Please provide a different email'
				})
			}


	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNumber !== '' && password1 !== '' && password2 !=='') && (password1 === password2)){
			setIsActive(true);

		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNumber, password1, password2])

	return(

		(user.id !==null) ?
			<Redirect to = '/courses' />
			:
		<Form onSubmit= {e=>registerUser(e)}>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control 
					type= 'firstName'
					placeholder='Please enter your first name here'
					value = {firstName}
					onChange = {e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control 
					type= 'lastName'
					placeholder='Please enter your last name here'
					value = {lastName}
					onChange = {e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control 
					type= 'email'
					placeholder='Please enter your email here'
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className = "text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control 
					type= 'mobileNumber'
					placeholder='Please enter your mobile number here'
					value = {mobileNumber}
					onChange = {e => setMobileNumber(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group controlId = "password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control
				type='password'
				placeholder = 'Please input your password here'
				value = {password1}
				onChange = {e => setPassword1(e.target.value)}
				required
				/>
			</Form.Group>

			<Form.Group controlId='password2'>
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control
				type='password'
				placeholder = 'Please verify your password'
				value = {password2}
				onChange = {e => setPassword2(e.target.value)}
				required
				/>
			</Form.Group>

			{ isActive ? 
				<Button variant = 'primary' type = 'submit' id = 'submitBtn'>
							Register
				</Button>

				:

				<Button variant = 'danger' type = 'submit' id='submitBtn' disabled>
					Register
				</Button>
			}
		</Form>

	)
}


