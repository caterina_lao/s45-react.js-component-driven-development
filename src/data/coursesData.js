const coursesData = [
{
	id: "wdc001",
	name: "PHP - Laravel",
	description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, harum deserunt modi odio aut nisi itaque similique aliquid dolor voluptates praesentium autem rerum culpa eaque cumque! Ea, animi mollitia accusamus!",
	price: 45000,
	onOffer: true
},
{
	id: "wdc002",
	name: "Python-Django",
	description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, harum deserunt modi odio aut nisi itaque similique aliquid dolor voluptates praesentium autem rerum culpa eaque cumque! Ea, animi mollitia accusamus!",
	price: 55000,
	onOffer: true
},
{
	id: "wdc003",
	name: "Java - Springboot",
	description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, harum deserunt modi odio aut nisi itaque similique aliquid dolor voluptates praesentium autem rerum culpa eaque cumque! Ea, animi mollitia accusamus!",
	price: 60000,
	onOffer: true
}
] 

export default coursesData;